import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  header: {
    right: 80,
    marginLeft: 80,
    marginRight: 0,
    textAlign: 'left',
    width: '80%'
  }
});

const MainNavBar = (title = '') => ({ navigation }) => ({
  headerTitleStyle: StyleSheet.flatten(styles.header),
  headerStyle: {
    backgroundColor: 'white'
  },
  title: title.toUpperCase(),
  headerLeft: (
    <TouchableOpacity
      style={{ marginLeft: 16, marginRight: 24, width: 24, height: 24 }}
      onPress={() => navigation.navigate('DrawerOpen')}
    >
      <Image
        source={require('../icons/menu.png')}
        style={{ width: 24, height: 24 }}
      />
    </TouchableOpacity>
  )
});

export default MainNavBar;
