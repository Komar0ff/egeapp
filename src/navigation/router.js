import React from 'react';
import { View, Image, TouchableOpacity, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import {
  addNavigationHelpers,
  StackNavigator,
  DrawerNavigator,
  DrawerItems
} from 'react-navigation';

// screens
import Registration from '../screens/Registration';
import Verification from '../screens/Verification';
import StudentProfile from '../screens/StudentProfile';
import StudentMain from '../screens/StudentMain';
import TeacherProfile from '../screens/TeacherProfile';
import Info from '../screens/Info';
import SharePromocode from '../screens/Share';
import Promocode from '../screens/Promocode';
import Payment from '../screens/Payment';
import Subject from '../screens/Subject';
import SubjectDetails from '../screens/SubjectDetails';
import Course from '../screens/Course';
import SubCourse from '../screens/SubCourse';
import Events from '../screens/Events';
import Theme from '../screens/Theme';
import Theory from '../screens/Theory';
import Practice from '../screens/Practice';

import DrawerContent from '../components/DrawerContent';

import MainNavBar from './MainNavBar';
import NoneNavBar from './NoneNavBar';
import TeachingNavBar from './TeachingNavBar';
import CourseNavBar from './CourseNavBar';

const MainNavigator = StackNavigator(
  {
    Registration: {
      screen: Registration,
      navigationOptions: NoneNavBar
    },
    Verification: {
      screen: Verification,
      navigationOptions: NoneNavBar
    },
    StudentProfile: {
      screen: StudentProfile,
      navigationOptions: MainNavBar('Профиль')
    },
    TeacherProfile: {
      screen: TeacherProfile,
      navigationOptions: {
        title: 'Профиль преподавателя',
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    StudentMain: {
      screen: StudentMain,
      navigationOptions: MainNavBar('Обучение')
    },
    Info: {
      screen: Info,
      navigationOptions: MainNavBar('Информация')
    },
    Payment: {
      screen: Payment,
      navigationOptions: MainNavBar('Оплата')
    },
    Promocode: {
      screen: Promocode,
      navigationOptions: MainNavBar('Ввести промо-код')
    },
    Share: {
      screen: SharePromocode,
      navigationOptions: MainNavBar('Поделиться')
    },
    Subject: {
      screen: Subject,
      navigationOptions: TeachingNavBar()
    },
    SubjectDetails: {
      screen: SubjectDetails,
      navigationOptions: TeachingNavBar('О КУРСЕ')
    },
    Course: {
      screen: Course,
      navigationOptions: TeachingNavBar()
    },
    SubCourse: {
      screen: SubCourse,
      navigationOptions: CourseNavBar
    },
    Events: {
      screen: Events,
      navigationOptions: MainNavBar('События')
    },
    Theme: {
      screen: Theme,
      navigationOptions: CourseNavBar
    },
    Theory: {
      screen: Theory,
      navigationOptions: CourseNavBar
    },
    Practice: {
      screen: Practice,
      navigationOptions: CourseNavBar
    }
  },
  {
    cardStyle: {
      backgroundColor: 'white'
    }
  }
);

export const AppNavigator = DrawerNavigator(
  {
    Main: {
      screen: MainNavigator
    }
  },
  {
    contentComponent: props => <DrawerContent {...props} />
  }
);

const AppWithNavigationState = ({ dispatch, nav }) => {
  return (
    <AppNavigator
      navigation={addNavigationHelpers({
        dispatch,
        state: nav
      })}
    />
  );
};

const mapStateToProps = state => ({
  nav: state.nav,
  registered: state.user.phone
});

export default connect(mapStateToProps)(AppWithNavigationState);
