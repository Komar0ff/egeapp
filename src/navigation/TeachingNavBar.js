import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  header: {
    right: 80,
    marginLeft: 120,
    marginRight: 0,
    textAlign: 'left',
    width: '100%'
  }
});

const TeachingNavBar = title => ({ navigation }) => ({
  headerTitleStyle: StyleSheet.flatten(styles.header),
  headerStyle: {
    backgroundColor: 'white'
  },
  title: title ||
    `${navigation.state.params.name}`.toUpperCase(),
  headerLeft: (
    <TouchableOpacity
      style={{ marginLeft: 16, marginRight: 24, width: 24, height: 24 }}
      onPress={() => navigation.goBack(null)}
    >
      <Image
        source={require('../icons/back.png')}
        style={{ width: 24, height: 24 }}
      />
    </TouchableOpacity>
  )
});

export default TeachingNavBar;
