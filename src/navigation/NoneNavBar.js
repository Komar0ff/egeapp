import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const NoneNavBar = {
  headerStyle: {
    backgroundColor: 'white',
    shadowColor: 'transparent',
    shadowRadius: 0,
    shadowOffset: {
      height: 0
    },
    elevation: 0
  },
  title: '',
  headerLeft: null
};

export default NoneNavBar;
