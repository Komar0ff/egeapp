import { GET_THEMES } from '../actions'

const initialState = []

const themes = (state = initialState, action) => {
  switch (action.type) {
    case GET_THEMES:
      return action.themes;
    default:
      return state;
  }
};

export default themes;
