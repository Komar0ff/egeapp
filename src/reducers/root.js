import { combineReducers } from 'redux';

import nav from './nav';
import user from './user';
import courses from './courses';
import parts from './parts';
import themes from './themes';
import subthemes from './subthemes';
import test from './test';

const rootReducer = combineReducers({
  nav,
  user,
  courses,
  parts,
  themes,
  subthemes,
  test
});

export default rootReducer;
