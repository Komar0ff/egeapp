import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../navigation/router';

const INITIAL_STATE = AppNavigator.router.getStateForAction(
  NavigationActions.init()
);

const nav = (state = INITIAL_STATE, action) => {
  let nextState;
  if (action.type === 'persist/REHYDRATE') {
    if (action.payload.user && action.payload.user.phone !== '') {
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'StudentProfile' }),
        state
      );
    }
    if (action.payload.user && action.payload.user.profileIsSaved === true) {
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'StudentMain' })]
        }),
        state
      );
    }
  } else {
    nextState = AppNavigator.router.getStateForAction(action, state);
  }
  return nextState || state;
};

export default nav;
