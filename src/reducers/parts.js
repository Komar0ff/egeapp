import { GET_PARTS } from '../actions'

const initialState = []

const courses = (state = initialState, action) => {
  switch (action.type) {
    case GET_PARTS:
      return action.parts;
    default:
      return state;
  }
};

export default courses;
