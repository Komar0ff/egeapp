import { GET_TEST } from '../actions'

const initialState = []

const test = (state = initialState, action) => {
  switch (action.type) {
    case GET_TEST:
      return action.test;
    default:
      return state;
  }
};

export default test;
