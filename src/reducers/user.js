import { CREATE_USER, CHANGE_PROFILE, SAVE_PAYMENT, SAVE_PROFILE, CHANGE_PAYMENT } from '../actions';

const initialState = {
  phone: '',
  userType: 'student',
  profileIsSaved: false,
  paymentIsSaved: false,
  profile: {},
  payment: {}
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_USER:
      return {
        ...state,
        phone: action.phone,
        userType: action.userType
      };
    case CHANGE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          [action.field]: action.value
        }
      };
    case CHANGE_PAYMENT:
      return {
        ...state,
        payment: {
          ...state.payment,
          [action.field]: action.value
        }
      };
    case SAVE_PROFILE:
      return {
        ...state,
        profileIsSaved: action.profileIsSaved
      };
    case SAVE_PAYMENT:
      return {
        ...state,
        paymentIsSaved: action.paymentIsSaved
      };
    default:
      return state;
  }
};

export default user;
