import { GET_COURSES } from '../actions'

const initialState = []

const courses = (state = initialState, action) => {
  switch (action.type) {
    case GET_COURSES:
      return action.courses;
    default:
      return state;
  }
};

export default courses;
