import { GET_SUBTHEMES, THEORY_COMPLETED, TEST_COMPLETED } from '../actions';

const initialState = [];

const subthemes = (state = initialState, action) => {
  switch (action.type) {
    case GET_SUBTHEMES:
      return action.subthemes;
    case THEORY_COMPLETED:
      state.filter(item => item.id === action.id)[0].completeTheory = true;
      return state;
    case TEST_COMPLETED:
      state.filter(item => item.id === action.id)[0].completeTest = true;
      return state;
    default:
      return state;
  }
};

export default subthemes;
