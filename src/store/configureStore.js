import { createStore, applyMiddleware, compose } from 'redux';
import { AsyncStorage } from 'react-native';
import { persistStore, autoRehydrate } from 'redux-persist';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from '../reducers/root';

const enhancer = compose(applyMiddleware(thunk, logger), autoRehydrate());

const configureStore = initialState => {
  const store = createStore(rootReducer, initialState, enhancer);
  persistStore(store, {
    blacklist: ['nav'],
    whitelist: ['user'],
    storage: AsyncStorage
  });
  return store;
};

export default configureStore();
