import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

import Button from '../components/Button';

class Promocode extends React.Component {
  state = {
    promocode: ''
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Пожалуйста, введите промо-код в поле ниже</Text>
        <TextInput
          value={this.state.promocode}
          onChangeText={promocode => this.setState({ promocode })}
          maxLength={6}
          style={styles.input}
          autoCapitalize='none'
          autoCorrect={false}
        />
        <Button>
          отправить
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 60,
  },
  input: {
    fontSize: 26,
    marginHorizontal: '5%',
    marginTop: 20,
    backgroundColor: '#f5f5f5',
    height: 50,
    borderWidth: 1,
    borderColor: 'lightgrey',
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    marginHorizontal: '5%'
  }
});

export default Promocode;
