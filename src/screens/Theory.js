import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import HTMLView from 'react-native-htmlview';

import Title from '../components/Title';
import Content from '../components/Content';
import Button from '../components/Button';

import { theoryCompleted } from '../actions'

class Theory extends PureComponent {
  render() {
    const { navigate, goBack } = this.props.navigation;
    const { name, theory, description, subTheme, subthemeId, test } = this.props.navigation.state.params;
    const htmlContent = theory;
    // const lessonNum = subTheme.split('.')[1];
    // const lessonName = `Урок ${lessonNum}. ${subTheme.split(' ')[1]}`;
    return (
      <View style={{ flex: 1, paddingTop: 8 }}>
        <Text
          style={{
            marginBottom: 12,
            fontSize: 20,
            textAlign: 'center'
          }}
        >
          {subTheme}
        </Text>
        <Content>
          <View style={{ paddingTop: 10 }}>
            <HTMLView stylesheet={styles} value={htmlContent} />
          </View>
        </Content>
        <Button onPress={() => {
          this.props.theoryCompleted(subthemeId, true)
          navigate('Practice', {
            name: `Урок`,
            description,
            subTheme,
            subthemeId: item.id,
            test
          })
        }}>завершить</Button>
        <View style={{ height: 16 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 100,
    height: 100
  },
});

const mapDispatchToProps = dispatch => ({
  theoryCompleted: (id, isCompleted) => dispatch(theoryCompleted(id, isCompleted))
})

export default connect(null, mapDispatchToProps)(Theory);
