import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import InfoTeacher from '../components/InfoTeacher';

const Info = () => (
  <View style={styles.container}>
    <View style={styles.infoContainer}>
      <Text style={styles.infoText}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio accusantium doloremque voluptatum, suscipit repellat architecto. Possimus, quam. Perspiciatis, eaque. Eligendi maxime reiciendis ad aperiam optio numquam eaque aspernatur dolores itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci provident tenetur, tempora aliquid dolorem dicta iure voluptatum totam cum expedita laboriosam, laudantium perferendis inventore sit accusantium explicabo, minus neque deleniti.
      </Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  infoContainer: {
    marginTop: 20,
    marginBottom: 60,
    width: '90%',
    marginLeft: '5%'
  },
  infoText: {
    fontSize: 14,
    textAlign: 'center'
  },
  teacherContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    width: '90%',
    marginLeft: '5%'
  }
});

export default Info;
