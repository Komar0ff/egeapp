import React, { PureComponent } from 'react';
import { FlatList, ActivityIndicator, View } from 'react-native';

import Event from '../components/Event';

class Events extends PureComponent {
  state = {
    refreshing: false
  };

  // _refresh = () => {

  // }

  render() {
    const data = [
      {
        title: 'Название предмета',
        description: 'Статус или имя ученика',
        timeLeft: 6,
        id: 1
      },
      {
        title: 'Название предмета',
        description: 'Статус или имя ученика',
        timeLeft: 6,
        id: 2
      }
    ];
    return (
      <FlatList
        data={data}
        ListHeaderComponent={<View style={{ paddingTop: 8  }} />}
        renderItem={({ item }) => (
          <Event
            title={item.title}
            description={item.description}
            timeLeft={item.timeLeft}
          />
        )}
        keyExtractor={item => item.id}
        refreshing={this.state.refreshing}
        // onRefresh={this._refresh}
      />
    );
  }
}

export default Events;
