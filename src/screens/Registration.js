import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  Alert
} from 'react-native';
import Button from '../components/Button';

const MAX_LENGTH_CODE = 6;
const PHONE_NUMBER_LENGTH = 10;

const SMS_GATEWAY_LOGIN = 'z1500494926240';
const SMS_GATEWAY_PASSWORD = '505669';

const brandColor = '#744BAC';

function getRandomArbitrary(min, max) {
  return parseInt(Math.random() * (max - min) + min);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20
  },
  description: {
    fontSize: 18,
    textAlign: 'center'
  },
  form: {
    margin: 20
  },
  textInput: {
    height: 20,
    fontSize: 20,
    width: '40%'
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold'
  },
  callingCodeText: {
    paddingRight: 5,
    fontSize: 20
  }
});

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      phone: '',
      verificationCode: getRandomArbitrary(100000, 999999)
    };
    this.phone = 'placeholder';
  }

  _sendSms = (phone, text) => {
    // var url = `https://gate.iqsms.ru/send/?phone=${'%2B7' + phone}&text=${text}&login=${SMS_GATEWAY_LOGIN}&password=${SMS_GATEWAY_PASSWORD}`;
    // Alert.alert('URL', url);
    fetch('https://api.github.com/zen')
      .then(function(response) {
        if (response.status == 200) return;
        else throw new Error('Something went wrong on api server!');
      })
      .catch(function(error) {
        console.error(error);
      });
  };

  _getCode = function() {
    this._sendSms(this.state.phone, this.state.verificationCode);
    this.textInput.setNativeProps({ text: '' });
  };

  _onChangeText = val => {
    this.setState({ phone: val });
  };

  _tryAgain = () => {
    this.textInput.setNativeProps({ text: '' });
    this.textInput.focus();
    this.setState({ enterCode: false });
  };

  _getSubmitAction = () => {
    if (this.state.phone.length === PHONE_NUMBER_LENGTH) {
      this._getCode();
      this.props.navigation.navigate('Verification', {
        code: this.state.verificationCode,
        phone: this.state.phone
      });
    }
  };

  render() {
    const textStyle = {};
    return (
      <View style={styles.container}>
        <Text style={styles.header}>РЕГИСТРАЦИЯ</Text>
        <Text style={styles.description}>
          Для регистрации на ваш телефон будет отправленно SMS с кодом подтверждения
        </Text>
        <View style={styles.form}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text style={styles.callingCodeText}>+7</Text>
            <TextInput
              ref={c => {
                this.textInput = c;
              }}
              name={'phoneNumber'}
              type={'TextInput'}
              autoCapitalize={'none'}
              value={this.state.input}
              autoCorrect={false}
              onChangeText={this._onChangeText}
              underlineColorAndroid={'transparent'}
              placeholder={'Номер телефона'}
              keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
              style={[styles.textInput, textStyle]}
              returnKeyType="go"
              autoFocus
              selectionColor="black"
              maxLength={10}
              onSubmitEditing={this._getSubmitAction}
              keyboardType="phone-pad"
            />

          </View>
          <View style={{ height: 10, width: 200, marginTop: 50 }} />
          <Button onPress={this._getSubmitAction}>
            ОТПРАВИТЬ
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  phone: user.phone
});

export default connect(mapStateToProps)(Registration);
