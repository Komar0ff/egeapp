import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Schedule from '../components/Schedule';

const SubjectDetails = props => (
  <View>
    <Text>{props.navigation.state.params.name}</Text>
    <View style={{ backgroundColor: '#f2f2f2', height: 200 }}>
      <View style={styles.userDataBlock}>
        <Image
          style={{
            height: 60,
            width: 60,
            borderRadius: 30
          }}
          source={{
            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWBbf7xKVbM2LaMvUkL-bxgz1pvVUMB_4KwDSrJ5Ez1J9xnxGJ'
          }}
        />
        <View style={{ justifyContent: 'center', marginLeft: 30 }}>
          <Text style={{ fontSize: 20 }}>{props.studentName}</Text>
          <Text style={{ fontSize: 18 }}>{`Email: ${props.email}`}</Text>
          <Text style={{ fontSize: 18 }}>{`Phone: +7 ${props.phone}`}</Text>
        </View>
      </View>
      <Text>Математика ЕГЭ</Text>
      <Text>Русский ЕГЭ</Text>
    </View>
    <Schedule />
  </View>
);

const styles = StyleSheet.create({
  userDataBlock: {
    marginLeft: '10%',
    flexDirection: 'row',
    alignItems: 'center'
  }
});

const mapStateToProps = state => ({
  studentName: state.user.profile.name,
  email: state.user.profile.email,
  phone: state.user.phone
});

export default connect(mapStateToProps)(SubjectDetails);
