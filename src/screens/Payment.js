import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, TextInput, View, Image, ListView, TouchableOpacity } from 'react-native';

import { TextInputMask } from 'react-native-masked-text';
import Button from '../components/Button';
import { changePayment, savePayment } from '../actions';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash'

const brandColor = '#afafaf';

const styles = StyleSheet.create({
  textInput: {
    fontSize: 20,
    marginBottom: 5,
    height: 35,
    borderWidth: 1,
    borderColor: 'lightgrey',
    width: '65%'
  },
  textList: {
    fontSize: 20,
    height: 35
  },
  inputLabel: {
    marginBottom: 5,
    fontSize: 14
  },
  box1: {
    flex: 1
  },
  box2: {
    flex: 1
  },
  detailBox: {
    flexDirection: 'row'
  },
  button: {
    marginTop: 20,
    height: 50,
    width: '70%',
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#000000',
    fontSize: 16
  },
  button_container: {
    marginTop: 200
  }
});

class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: '',
      holder: '',
      isExpired: '',
      cvv: ''
    };
  }

  componentDidMount = () => {
    const initial = _.toPairs(this.state);
    initial.forEach(pair => {
      this.props.changePayment(pair[0], pair[1]);
    });
  };

  _changePayment = (value, field) => {
    this.setState({ [field]: value }, () => this.props.changePayment(field, value));
  };

  render() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'StudentMain' })]
    });
    return (
      <View style={{ marginTop: 50 }}>
        <View style={{ paddingLeft: 30, height: '50%' }}>
          <Text style={styles.inputLabel}>Номер</Text>
          <TextInputMask
            type="credit-card"
            onChangeText={(value, field) => this._changePayment(value, 'card')}
            value={this.state.card}
            maxLength={19}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Владелец</Text>
          <TextInput
            value={this.state.holder}
            onChangeText={(value, field) => this._changePayment(value, 'holder')}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>До</Text>
          <TextInputMask
            type="datetime"
            options={{ format: 'MM/YY' }}
            value={this.state.isExpired}
            onChangeText={(value, field) => this._changePayment(value, 'isExpired')}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>CVV</Text>
          <TextInput
            value={this.state.cvv}
            onChangeText={(value, field) => this._changePayment(value, 'cvv')}
            style={styles.textInput}
            maxLength={3}
            secureTextEntry
          />
        </View>
        <View style={styles.button_container}>
          <Button
            onPress={() => {
              this.props.savePayment(true);
              this.props.navigation.dispatch(resetAction);
            }}
          >
            добавить
          </Button>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  changePayment: (field, value) => dispatch(changePayment(field, value)),
  savePayment: paymentIsSaved => dispatch(savePayment(paymentIsSaved))
});

export default connect(null, mapDispatchToProps)(Payment);
