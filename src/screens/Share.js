import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Share } from 'react-native';

import Button from '../components/Button';

const background = '#ffffff';
const textColor = '#000000';
const brandColor = '#744BAC';
const buttonTextColor = '#fff';

const SharePromocode = () => (
  <View style={styles.container}>
    <Text style={styles.paragraph}>
      Ваш промо-код:
    </Text>
    <Text style={styles.promo_code}>
      80e7f
    </Text>
    <View style={styles.footerContainer}>
      <Text style={styles.footerParagraph}>
        Отправьте промо-код вашим друзьям, чтобы получить бесплатные занятия (1 приглашенный друг = 1 бесплатное занятие)
      </Text>
      <Button
        onPress={() =>
          Share.share(
            {
              message: '80e7f',
              url: 'https://vk.com/shashkovdanil',
              title: 'Ваш промо-код'
            },
            {
              dialogTitle: 'Поделиться',
              tintColor: 'green',
              excludedActivityTypes: ['com.apple.mobilenotes.SharingExtension']
            }
          )}
      >
        Поделиться
      </Button>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 24,
    backgroundColor: background
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 24,
    backgroundColor: background
  },
  footerContainer: {
    flex: 1,
    paddingTop: 24,
    backgroundColor: background
  },
  icon: {
    marginLeft: 25,
    marginRight: 10
  },
  navText: {
    margin: 5,
    fontSize: 18,
    textAlign: 'center',
    color: textColor
  },
  paragraph: {
    margin: 0,
    fontSize: 18,
    textAlign: 'center',
    color: textColor
  },
  promo_code: {
    marginTop: 15,
    marginBottom: 200,
    fontSize: 50,
    fontWeight: 'bold',
    textAlign: 'center',
    color: textColor
  },
  footerParagraph: {
    margin: 0,
    fontSize: 18,
    textAlign: 'center',
    color: textColor
  },
  button: {
    marginTop: 20,
    marginBottom: 20,
    padding: 10,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  },
  buttonText: {
    color: buttonTextColor,
    fontSize: 16,
    fontWeight: 'bold'
  }
});

export default SharePromocode;
