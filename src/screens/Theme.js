import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';

import Title from '../components/Title';
import { fetchSubthemes, fetchTest } from '../actions';

const checked = require('../icons/checked.png');

class Theme extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      completeTheory: this.props.completeTheory,
      completeTest: this.props.completeTest
    };
  }
  componentDidMount() {
    this.props.fetchTest(this.props.navigation.state.params.item.id);
  }
  componentWillReceiveProps(nextProps) {
    console.log(this.props.completeTheory, nextProps.completeTheory);
    if (this.props.completeTheory !== nextProps.completeTheory) {
      this.setState({ completeTheory: nextProps.completeTheory });
    }
    if (this.props.completeTest !== nextProps.completeTest) {
      this.setState({ completeTest: nextProps.completeTest });
    }
  }
  render() {
    console.log(this.props);
    const { navigate } = this.props.navigation;
    const { name, description, subTheme, theory, item } = this.props.navigation.state.params;
    const { completeTheory, completeTest } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Text
          style={{
            marginTop: 8,
            marginBottom: 38,
            fontSize: 28,
            textAlign: 'center'
          }}
        >
          {subTheme}
        </Text>
        <TouchableOpacity
          onPress={() =>
            navigate('Theory', {
              name: `Подготовка к уроку`,
              description,
              subTheme,
              theory,
              subthemeId: item.id,
              test: this.props.test
            })}
          style={styles.lessonBlock}
        >
          <View style={styles.textForLesson}>
            <Text
              style={[styles.subjectInLessonBlock, { textDecorationLine: completeTheory ? 'line-through' : 'none' }]}
            >
              Подготовка к уроку
            </Text>
          </View>
          {completeTheory
            ? <View style={styles.progressForLesson}>
                <Image source={checked} style={{ width: 32, height: 32 }} />
              </View>
            : null}
        </TouchableOpacity>
        <TouchableOpacity style={styles.lessonBlock}>
          <View style={styles.textForLesson}>
            <Text style={[styles.subjectInLessonBlock]}>Урок</Text>
          </View>
          {completeTest
            ? <View style={styles.progressForLesson}>
                <Text style={styles.subjectInLessonBlock}>Зачет</Text>
              </View>
            : null}
        </TouchableOpacity>
        <TouchableOpacity style={styles.lessonBlock}>
          <View style={styles.textForLesson}>
            <Text style={styles.subjectInLessonBlock}>Домашняя работа</Text>
          </View>
          {/*{complete
            ? <View style={styles.progressForLesson}>
                <Text style={styles.subjectInLessonBlock}>Зачет</Text>
              </View>
            : null}*/}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  lessonBlock: {
    flexDirection: 'row',
    height: 75,
    marginHorizontal: '10%',
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#c4c4c4'
  },
  textForLesson: {
    flex: 3,
    justifyContent: 'center'
  },
  progressForLesson: {
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subjectInLessonBlock: {
    margin: 4,
    marginLeft: 15,
    fontSize: 20,
    textAlign: 'left'
  },
  isMainSubjectText: {
    margin: 4,
    marginTop: 0,
    marginLeft: 15,
    fontSize: 13,
    textAlign: 'left',
    color: 'grey'
  }
});

const mapDispatchToProps = dispatch => ({
  fetchSubthemes: id => dispatch(fetchSubthemes(id)),
  fetchTest: id => dispatch(fetchTest(id))
});

const mapStateToProps = (state, ownProps) => {
  const { item } = ownProps.navigation.state.params;
  const { id } = item;
  const completeTheory = state.subthemes.filter(i => i.id === id)[0].completeTheory;
  const completeTest = state.subthemes.filter(i => i.id === id)[0].completeTest;
  return {
    completeTheory,
    completeTest,
    test: state.test
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Theme);
