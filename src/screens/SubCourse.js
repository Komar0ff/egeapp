import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import { fetchSubthemes, fetchTest } from '../actions'

import Title from '../components/Title';
import LessonRow from '../components/LessonRow';

class SubCourse extends Component {
  componentDidMount() {
    this.props.fetchSubthemes(this.props.navigation.state.params.courseId);
  }
  render() {
    const courseId = this.props.navigation.state.params.courseId
    return (
      <View style={{ flex: 1, paddingTop: 8  }}>
        <FlatList
          data={this.props.subthemes}
          renderItem={({ item }) => (
            <LessonRow
              onPress={() => {this.props.navigation.navigate('Theme', {
                name: `${this.props.navigation.state.params.name} | ${item.name}`,
                description: this.props.navigation.state.params.description,
                subTheme: item.name,
                courseId,
                item,
                theory: item.theory
              })}}
              title={item.name}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  fetchSubthemes: id => dispatch(fetchSubthemes(id)),
  fetchTest: id => dispatch(fetchTest(id))
});

const mapStateToProps = ({ subthemes }) => ({
  subthemes
})

export default connect(mapStateToProps, mapDispatchToProps)(SubCourse);
