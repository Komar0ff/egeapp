import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TextInputMask } from 'react-native-masked-text';
import ImagePicker from 'react-native-image-picker';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  Image,
  Alert,
  ScrollView,
  PixelRatio
} from 'react-native';
import _ from 'lodash';
import { NavigationActions } from 'react-navigation';

import Button from '../components/Button';

import { changeProfile, saveProfile } from '../actions';

const brandColor = '#744BAC';

const styles = StyleSheet.create({
  textInput: {
    fontSize: 20,
    marginBottom: 10,
    height: 35
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    borderRadius: 40,
    width: 80,
    height: 80
  },
  inputLabel: {
    fontSize: 14
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold'
  }
});

class StudentProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: 'Иван Иванов',
      email: 'some@gmail.com',
      phone: this.props.phone,
      address: 'г. Москва, ул. Москворечье, д.7, к. 8, кв. 119',
      parentName: 'Иван Иванов',
      parentPhone: '79999999999',
      avatarSource: null
    };
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  componentDidMount = () => {
    const initial = _.toPairs(this.state);
    initial.forEach(pair => {
      this.props.changeProfile(pair[0], pair[1]);
    });
  };

  _changeProfile = (value, field) => {
    this.setState({ [field]: value }, () =>
      this.props.changeProfile(field, value)
    );
  };

  render() {
    const mask = '+9 (999) 999-99-99';
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'StudentMain' })]
    });
    return (
      <View style={{ paddingLeft: 20, paddingRight: 20 }}>
        <ScrollView>
          <TouchableOpacity
            style={{ alignItems: 'center', justifyContent: 'center', marginTop: 8 }}
            onPress={this.selectPhotoTapped.bind(this)}
          >
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                { marginBottom: 20 }
              ]}
            >
              {this.state.avatarSource === null
                ? <Text style={{ textAlign: 'center' }}>Выберите фото</Text>
                : <Image
                    style={styles.avatar}
                    source={this.state.avatarSource}
                  />}
            </View>
          </TouchableOpacity>

          <Text style={styles.inputLabel}>Полное имя</Text>
          <TextInput
            onChangeText={(value, field) => this._changeProfile(value, 'name')}
            value={this.state.name}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Email</Text>
          <TextInput
            onChangeText={(value, field) => this._changeProfile(value, 'email')}
            value={this.state.email}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Телефон</Text>
          <TextInputMask
            type="cel-phone"
            onChangeText={(value, field) => this._changeProfile(value, 'phone')}
            value={this.state.phone}
            options={{ dddMask: mask }}
            maxLength={mask.length}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Адрес</Text>
          <TextInput
            onChangeText={(value, field) =>
              this._changeProfile(value, 'address')}
            value={this.state.address}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Имя родителя</Text>
          <TextInput
            onChangeText={(value, field) =>
              this._changeProfile(value, 'parentName')}
            value={this.state.parentName}
            style={styles.textInput}
          />

          <Text style={styles.inputLabel}>Телефон родителя</Text>
          <TextInputMask
            type="cel-phone"
            onChangeText={(value, field) =>
              this._changeProfile(value, 'parentPhone')}
            value={this.state.parentPhone}
            options={{ dddMask: mask }}
            maxLength={mask.length}
            style={styles.textInput}
          />
          <Button
            onPress={() => {
              this.props.saveProfile(true);
              this.props.navigation.dispatch(resetAction);
            }}
          >
            СОХРАНИТЬ
          </Button>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  phone: `7${user.phone}`
});

const mapDispatchToProps = dispatch => ({
  changeProfile: (field, value) => dispatch(changeProfile(field, value)),
  saveProfile: profileIsSaved => dispatch(saveProfile(profileIsSaved))
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentProfile);
