import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';

import Title from '../components/Title';
import LessonRow from '../components/LessonRow';

import { fetchThemes } from '../actions';

class Course extends Component {
  componentDidMount() {
    this.props.fetchThemes(this.props.navigation.state.params.courseId);
  }
  render() {
    const courseId = this.props.navigation.state.params.courseId
    return (
      <View style={{ flex: 1 }}>
        <Title title={this.props.navigation.state.params.title} description={this.props.navigation.state.params.description} />
        <FlatList
          data={this.props.themes}
          renderItem={({ item }) =>
            <LessonRow
              onPress={() =>
                this.props.navigation.navigate('SubCourse', {
                  courseId,
                  description: this.props.navigation.state.params.description,
                  name: item.name
                })}
              title={item.name}
            />}
          keyExtractor={({ id }) => id}
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchThemes: id => dispatch(fetchThemes(id))
});

const mapStateToProps = ({ themes }) => ({
  themes
})

export default connect(mapStateToProps, mapDispatchToProps)(Course);
