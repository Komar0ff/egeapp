import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux'
import moment from 'moment';
import ruLocale from 'moment/locale/ru';
import ProgressCircle from 'react-native-progress-circle';

import { fetchCourses } from '../actions'

const background = '#fff';
const marginFromCorner = 25;

class Subject extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.nav.navigate('Subject', {
          name: this.props.name,
          id: this.props.id
        })}
        style={styles.lessonBlock}
      >
        <View style={styles.textForLesson}>
          <Text style={styles.subjectInLessonBlock}>
            {this.props.name}
          </Text>
          <Text style={styles.isMainSubjectText}>
            {this.props.profil}
          </Text>
        </View>
        <View style={styles.progressForLesson}>
          <ProgressCircle
            percent={12 / 80 * 100}
            radius={25}
            borderWidth={4}
            color="#7a7a7a"
            shadowColor="#e5e5e5"
            bgColor="#fff"
          >
            <Text style={{ fontSize: 12 }}>12/80</Text>
          </ProgressCircle>
        </View>
      </TouchableOpacity>
    );
  }
}

// class SubjectList extends Component {
//   render() {
//     return (
//       <View style={styles.subjectHolder}>
//         <FlatList
//           data={[{ key: ' Математика (ЕГЭ)' }, { key: 'Русский язык (ЕГЭ)' }]}
//           renderItem={({ item }) => (
//             <View style={styles.subjectHolderInsideView}>
//               <Text style={styles.subjectHolderInsideText}>{item.key}</Text>
//             </View>
//           )}
//         />
//       </View>
//     );
//   }
// }

// class Today extends Component {
//   componentDidMount() {
//     moment.updateLocale('ru', ruLocale);
//   }
//   render() {
//     const today = moment().format('D MMMM YYYY dddd').split(' ');
//     return (
//       <View style={styles.dateHolder}>
//         <View style={styles.dayDate}>
//           <Text style={styles.dayDateText}>{today[0]}</Text>
//         </View>
//         <View style={styles.monthDate}>
//           <Text style={styles.monthDateText}>{`${today[1]} ${today[2]}`}</Text>
//           <Text style={styles.monthDateText}>{today[3]}</Text>
//         </View>
//       </View>
//     );
//   }
// }

class StudentMain extends Component {
  componentDidMount() {
    this.props.fetchCourses()
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.subjectView}>
          <FlatList
            data={this.props.courses}
            renderItem={({ item }) => <Subject nav={this.props.navigation} name={item.name} id={item.id} />}
            keyExtractor={({ id }) => id}
          />
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchCourses: () => dispatch(fetchCourses())
})

const mapStateToProps = ({ courses }) => ({
  courses
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: background,
    paddingTop: 8
  },
  header: {
    flex: 1,
    marginLeft: 20,
    backgroundColor: 'grey'
  },
  subjectView: {
    flex: 5,
    marginLeft: marginFromCorner,
    marginRight: marginFromCorner
  },
  footerContainer: {
    flex: 4,
    justifyContent: 'flex-end',
    marginLeft: marginFromCorner,
    marginRight: marginFromCorner,
    marginBottom: 20
  },
  lessonBlock: {
    flexDirection: 'row',
    height: 75,
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1,
    borderRadius: 5
  },
  textForLesson: {
    flex: 3
  },
  progressForLesson: {
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subjectInLessonBlock: {
    margin: 4,
    marginLeft: 15,
    fontSize: 20,
    textAlign: 'left'
  },
  isMainSubjectText: {
    margin: 4,
    marginTop: 0,
    marginLeft: 15,
    fontSize: 13,
    textAlign: 'left',
    color: 'grey'
  },
  dateHolder: {
    flexDirection: 'row',
    marginLeft: 60,
    marginRight: 60,
    borderWidth: 1,
    borderBottomWidth: 0,
    borderRadius: 5
  },
  dayDate: {
    margin: 5
  },
  dayDateText: {
    fontSize: 40
  },
  monthDate: {
    marginTop: 10
  },
  monthDateText: {
    fontSize: 15
  },
  subjectHolder: {
    height: 125,
    borderWidth: 1,
    borderRadius: 5
  },
  subjectHolderInsideView: {
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 1
  },
  subjectHolderInsideText: {
    margin: 5,
    marginLeft: 15
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentMain)
