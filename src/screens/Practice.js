import React, { PureComponent } from 'react';
import { View, Text, TextInput } from 'react-native';
import HTMLView from 'react-native-htmlview';
import { connect } from 'react-redux'

import Title from '../components/Title';
import Content from '../components/Content';
import Button from '../components/Button';
import { testCompleted } from '../actions'

class Practice extends PureComponent {
  state = {
    answer: ''
  }
  render() {
    const { navigate, goBack } = this.props.navigation;
    const { title, description, subTheme, test, subthemeId } = this.props.navigation.state.params;
    const htmlContent = test._task_rendered;
    console.log(test);
    return (
      <View style={{ flex: 1, paddingTop: 8 }}>
        <Text
          style={{
            marginBottom: 12,
            fontSize: 20,
            textAlign: 'center'
          }}
        >
          {subTheme}
        </Text>
        <Text
          style={{
            marginBottom: 12,
            fontSize: 20,
            textAlign: 'center'
          }}
        >
          Задание № {/* номер задания */}
        </Text>
        <Content>
          <View style={{ paddingHorizontal: 10, paddingTop: 10 }}>
            <HTMLView value={htmlContent} />
          </View>
        </Content>
        <TextInput
          value={this.state.answer}
          onChangeText={(answer) => this.setState({ answer })}
          style={{
            fontSize: 20,
            marginBottom: 5,
            height: 35,
            borderWidth: 1,
            borderColor: 'lightgrey',
            marginLeft: '5%',
            marginTop: 10,
            width: '65%'
          }}
        />
        <Button onPress={() => {
          if (test.answers[0].content.toLowerCase() === this.state.answer.toLowerCase()) {
            this.props.testCompleted(subthemeId, true)
            goBack()
          }
        }}>далее</Button>
        <View style={{ height: 16 }} />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  testCompleted: (id, isCompleted) => dispatch(testCompleted(id, isCompleted))
})

export default connect(null, mapDispatchToProps)(Practice);
