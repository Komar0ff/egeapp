import React, { Component } from 'react';
import { Text, View, StyleSheet, Alert, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import ProgressCircle from 'react-native-progress-circle';

import Button from '../components/Button';

import { fetchParts } from '../actions';

class Subject extends Component {
  _handleButtonPress = () => {
    Alert.alert('Button pressed!', 'You did it!');
  };

  componentDidMount() {
    this.props.fetchParts(this.props.navigation.state.params.id);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.header_row}>
            <Text style={styles.header_light}>Преподаватель: Преподавалкин С.</Text>
          </View>
          <View style={styles.scores}>
            <View style={styles.scores_col}>
              <Text style={styles.scores_bold}> ЦЕЛЬ </Text>
              <View style={styles.scores_round_block}>
                <Text style={styles.mark}> 80 </Text>
                <Text style={styles.mark_word}> баллов </Text>
              </View>
            </View>
            <View style={styles.scores_col}>
              <Text style={styles.scores_bold}> СЕЙЧАС </Text>
              <View style={styles.scores_round_block}>
                <Text style={styles.mark}> 64 </Text>
                <Text style={styles.mark_word}> балла </Text>
              </View>
            </View>
          </View>
          <View style={styles.parts}>
            {this.props.parts.map(part =>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('Course', {
                    name: this.props.navigation.state.params.name,
                    description: part.name,
                    courseId: part.courseId
                  })}
                style={styles.parts_box}
                key={part.id}
              >
                <Text style={styles.parts_text}>{part.name}</Text>
                <ProgressCircle
                  percent={12 / 80 * 100}
                  radius={18}
                  borderWidth={3}
                  color="#7a7a7a"
                  shadowColor="#e5e5e5"
                  bgColor="#fff"
                >
                  <Text style={{ fontSize: 8 }}>12/80</Text>
                </ProgressCircle>
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.feedback}>
            <Button
              onPress={() =>
                this.props.navigation.navigate('SubjectDetails', {
                  lessonName: this.props.navigation.state.params.lessonName,
                  isProfile: this.props.navigation.state.params.isProfile
                })}
            >
              Подробнее о курсе
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header_row: {
    paddingVertical: 16,
    paddingLeft: '5%',
    justifyContent: 'center'
  },
  header_bold: {
    fontSize: 20,
    color: '#000'
  },
  header_light: {
    fontSize: 18,
    color: '#000'
  },
  scores: {
    flexDirection: 'row',
    width: '100%'
  },
  scores_col: {
    height: 180,
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f2f2f2'
  },
  scores_bold: {
    fontSize: 20,
    color: '#000',
    marginBottom: 5
  },
  scores_round_block: {
    marginLeft: 60,
    marginRight: 60,
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#c4c4c4',
    borderRadius: 50
  },
  mark: {
    fontSize: 40,
    backgroundColor: 'transparent'
  },
  mark_word: {
    backgroundColor: 'transparent'
  },
  parts: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    paddingBottom: 15
  },
  parts_box: {
    height: 70,
    flexDirection: 'row',
    marginBottom: 10,
    borderColor: '#ddd',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15
  },
  parts_box_last: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#ddd',
    borderWidth: 1,
    paddingLeft: 15,
    paddingRight: 15
  },
  parts_text: {
    fontSize: 20
  },
  parts_counter: {
    backgroundColor: '#aaa',
    borderRadius: 50,
    height: 50,
    width: 50
  },
  schedule: {
    flex: 3,
    paddingTop: 15,
    backgroundColor: '#ddd',
    flexDirection: 'row',
    paddingLeft: 15
  },
  schedule_col_1: {
    flex: 1,
    marginRight: 5
  },
  a1: {
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderColor: '#808080'
  },
  schedule_col_2: {
    flex: 7
  },
  schedule_col_3: {
    flex: 1,
    marginRight: 5
  },
  schedule_col_4: {
    flex: 6
  },
  day_of_the_week: {
    fontSize: 12,
    marginBottom: 5
  },
  day_of_the_week_bold: {
    fontWeight: 'bold',
    marginBottom: 5
  },
  feedback: {
    flex: 2,
    justifyContent: 'center',
    paddingBottom: 20
  },
  button: {
    flex: 1,
    marginBottom: 10,
    marginTop: 10,
    backgroundColor: '#aaa',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold'
  }
});

const mapDispatchToProps = dispatch => ({
  fetchParts: id => dispatch(fetchParts(id))
});

const mapStateToProps = ({ parts }) => ({
  parts
});

export default connect(mapStateToProps, mapDispatchToProps)(Subject);
