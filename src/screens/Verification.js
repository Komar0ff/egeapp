import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  Alert,
  AsyncStorage
} from 'react-native';

import { createUser } from '../actions';

const MAX_LENGTH_CODE = 6;
const PHONE_NUMBER_LENGTH = 10;

const brandColor = '#000';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: brandColor
  },
  description: {
    textAlign: 'center',
    color: '#000'
  },
  form: {
    margin: 20
  },
  textInput: {
    padding: 0,
    margin: 5,
    height: 20,
    flex: 1,
    fontSize: 20,
    color: brandColor
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold'
  },
  callingCodeText: {
    fontSize: 20,
    color: brandColor,
    paddingRight: 10,
    paddingTop: 3
  }
});

class Verification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      code: ''
    };
    this.phone = 'placeholder';
  }

  _load_required_window = () => {
    AsyncStorage.getItem('user_phone').then(value => {
      if (value) {
        fetch(`http://88.212.235.203/repet-api/is_teacher?phone=${value}`)
          .then(response => response.json())
          .then(responseJson => {
            console.log(responseJson);
            if (responseJson.is_teacher === 'False') {
              this.props.navigation.navigate('StudentProfile');
            } else {
              this.props.navigation.navigate('TeacherProfile');
            }
          })
          .catch(error => {
            console.error(error);
          });
      }
    });
  }

  _register = phone => {
    Alert.alert('Register', phone);
    AsyncStorage.setItem('user_phone', phone).then(() => {
      this._load_required_window();
    });
  };

  _verifyCode = () => {
    if (this.state.code == this.props.navigation.state.params.code) {
      this.props.createUser(this.props.navigation.state.params.phone, 'student');
      this.props.navigation.navigate('StudentProfile');
    } else {
      Alert.alert(
        '',
        this.state.code + ' != ' + this.props.navigation.state.params.code
      );
    }

    this.textInput.blur();
  };

  _onChangeText = val => {
    this.setState({ code: val });

    setTimeout(() => {
      if (val.length === MAX_LENGTH_CODE) {
        this._verifyCode();
      }
    }, 100);
  };

  _tryAgain = () => {
    this.textInput.setNativeProps({ text: '' });
    this.textInput.focus();
    this.setState({ enterCode: false });
  };

  render() {
    let textStyle = {
      height: 50,
      textAlign: 'center',
      fontSize: 40,
      fontWeight: 'bold'
    };

    return (
      <View style={styles.container}>
        <Text style={styles.header}>ПОДТВЕРЖДЕНИЕ</Text>
        <Text style={styles.description}>
          На указанный телефон было направленно SMS с кодом. Чтобы завершить регистрацию, пожалуйста, введите 6-значный код активации.
        </Text>
        <View style={styles.form}>
          <View style={{ flexDirection: 'row' }}>
            <TextInput
              ref={c => {
                this.textInput = c;
              }}
              name={'code'}
              type={'TextInput'}
              autoCapitalize={'none'}
              value={this.state.input}
              autoCorrect={false}
              onChangeText={this._onChangeText}
              underlineColorAndroid={'transparent'}
              placeholder={'_ _ _ _ _ _'}
              keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
              style={[styles.textInput, textStyle, { marginBottom: 20 }]}
              returnKeyType="go"
              autoFocus
              placeholderTextColor={brandColor}
              selectionColor={brandColor}
              maxLength={6}
              onSubmitEditing={this._getSubmitAction}
            />
          </View>
          <View style={{ height: 10, width: 200 }} />
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  createUser: (phone, userType) => dispatch(createUser(phone, userType))
});

export default connect(null, mapDispatchToProps)(Verification);
