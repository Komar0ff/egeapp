export const CREATE_USER = 'CREATE_USER';
export const createUser = (phone, userType) => ({
  type: CREATE_USER,
  phone,
  userType
});

export const CHANGE_PROFILE = 'CHANGE_PROFILE';
export const changeProfile = (field, value) => ({
  type: CHANGE_PROFILE,
  field,
  value
});

export const CHANGE_PAYMENT = 'CHANGE_PAYMENT';
export const changePayment = (field, value) => ({
  type: CHANGE_PAYMENT,
  field,
  value
});

export const SAVE_PROFILE = 'SAVE_PROFILE';
export const saveProfile = profileIsSaved => ({
  type: SAVE_PROFILE,
  profileIsSaved
});

export const SAVE_PAYMENT = 'SAVE_PAYMENT';
export const savePayment = paymentIsSaved => ({
  type: SAVE_PAYMENT,
  paymentIsSaved
});

export const GET_COURSES = 'GET_COURSES'
export const getCourses = courses => ({
  type: GET_COURSES,
  courses: courses.map(course => ({ name: course.name, id: course.id }))
})

export const FETCH_COURSES = 'FETCH_COURSES';
export const fetchCourses = () => async (dispatch) => {
  const res = await fetch('http://127.0.0.1:8000/repet-api/courses/all')
  const courses = await res.json()
  dispatch(getCourses(courses))
}

export const GET_PARTS = 'GET_PARTS'
export const getParts = parts => ({
  type: GET_PARTS,
  parts: parts.map(part => ({
    courseId: part.course_id,
    id: part.id,
    name: part.name,
    num: part.num
  }))
})

export const FETCH_PARTS = 'FETCH_PARTS';
export const fetchParts = id => async (dispatch) => {
  const res = await fetch(`http://127.0.0.1:8000/repet-api/courses/${id}/parts`)
  const parts = await res.json()
  dispatch(getParts(parts))
}

export const GET_THEMES = 'GET_THEMES'
export const getThemes = themes => ({
  type: GET_THEMES,
  themes: themes.map(theme => ({
    partId: theme.course_part_id,
    id: theme.id,
    name: theme.name,
    num: theme.num
  }))
})

export const FETCH_THEMES = 'FETCH_THEMES';
export const fetchThemes = id => async (dispatch) => {
  console.log(id, 123);
  const res = await fetch(`http://127.0.0.1:8000/repet-api/courses/${id}/themes`)
  const themes = await res.json()
  dispatch(getThemes(themes))
}

export const GET_SUBTHEMES = 'GET_SUBTHEMES'
export const getSubthemes = subthemes => ({
  type: GET_SUBTHEMES,
  subthemes: subthemes.map(subtheme => ({
    themeId: subtheme.theme_id,
    id: subtheme.id,
    name: subtheme.name,
    theory: subtheme.theory,
    completeTheory: false,
    completeTest: false
  }))
})

export const FETCH_SUBTHEMES = 'FETCH_SUBTHEMES';
export const fetchSubthemes = id => async (dispatch) => {
  const res = await fetch(`http://127.0.0.1:8000/repet-api/courses/${id}/subthemes`)
  const subthemes = await res.json()
  dispatch(getSubthemes(subthemes))
}

export const GET_TEST = 'GET_TEST'
export const getTest = test => ({
  type: GET_TEST,
  test
})

export const FETCH_TEST = 'FETCH_TEST'
export const fetchTest = id => async (dispatch) => {
  const res = await fetch(`http://127.0.0.1:8000/repet-api/courses/${id}/test`)
  const test = await res.json()
  dispatch(getTest(test))
}

export const THEORY_COMPLETED = 'THEORY_COMPLETED'
export const theoryCompleted = (id, isCompleted) => ({
  type: THEORY_COMPLETED,
  id,
  isCompleted
})

export const TEST_COMPLETED = 'TEST_COMPLETED'
export const testCompleted = (id, isCompleted) => ({
  type: TEST_COMPLETED,
  id,
  isCompleted
})
