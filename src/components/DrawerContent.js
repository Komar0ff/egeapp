import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import DrawerButton from './DrawerButton';

// icons
const events = require('../icons/calendar.png');
const exit = require('../icons/exit.png');
const info = require('../icons/info.png');
const payment = require('../icons/payment.png');
const profile = require('../icons/profile.png');
const promo = require('../icons/promo.png');
const share = require('../icons/share.png');
const study = require('../icons/study.png');

class DrawerContent extends Component {
  render() {
    const reset = screen => {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: screen })]
      });
      this.props.navigation.dispatch(resetAction);
    };
    const teacher = [
      {
        title: 'Текущие события',
        icon: events,
        onPress: () => reset('Events')
      },
      {
        title: 'Обучение',
        icon: study,
        onPress: () => reset('TeacherMain')
      },
      {
        title: 'Профиль',
        icon: profile,
        onPress: () => reset('Profile')
      },
      {
        title: 'Оплата',
        icon: payment,
        onPress: () => reset('Payment')
      },
      {
        title: 'Информация',
        icon: info,
        onPress: () => reset('Info')
      }
    ];
    const student = [
      {
        title: 'Текущие события',
        icon: events,
        onPress: () => reset('Events')
      },
      {
        title: 'Учиться',
        icon: study,
        onPress: () => reset('StudentMain')
      },
      {
        title: 'Профиль',
        icon: profile,
        onPress: () => reset('StudentProfile')
      },
      {
        title: 'Оплата',
        icon: payment,
        onPress: () => reset('Payment')
      },
      {
        title: 'Ввести промо-код',
        icon: promo,
        onPress: () => reset('Promocode')
      },
      {
        title: 'Поделиться',
        icon: share,
        onPress: () => reset('Share')
      },
      {
        title: 'Информация',
        icon: info,
        onPress: () => reset('Info')
      }
    ];
    const { container, button, buttonTitle } = styles;
    return (
      <View style={container}>
        <View style={{ height: '90%' }}>
          {this.props.type === 'student'
            ? student.map(button => (
                <DrawerButton
                  key={button.title}
                  Icon={button.icon}
                  onPress={button.onPress}
                >
                  {button.title}
                </DrawerButton>
              ))
            : teacher.map(button => (
                <DrawerButton
                  key={button.title}
                  Icon={button.icon}
                  onPress={button.onPress}
                >
                  {button.title}
                </DrawerButton>
              ))}
        </View>
        <DrawerButton
          Icon={exit}
          onPress={() => this.props.navigation.navigate('Registration')}
        >
          Выйти
        </DrawerButton>
      </View>
    );
  }
}

const styles = {
  container: {
    height: '100%',
    flex: 1,
    marginTop: 32,
    marginLeft: 16
  },
  name: {
    fontSize: 20,
    fontWeight: '500',
    color: '#fff'
  }
};

const mapStateToProps = ({ user }) => ({
  type: user.userType
});

export default connect(mapStateToProps)(DrawerContent);
