import React from 'react';
import { ScrollView, View } from 'react-native';

const Content = ({ children }) => (
  <View
    style={{
      backgroundColor: '#f2f2f2',
      marginHorizontal: '5%',
      paddingHorizontal: '5%',
      flex: 1
    }}
  >
    <ScrollView>
      {children}
    </ScrollView>
  </View>
);

export default Content;
