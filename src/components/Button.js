import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const brandColor = '#afafaf';

const Button = ({ children, onPress }) => (
  <TouchableOpacity style={styles.button} onPress={onPress}>
    <Text style={styles.buttonText}>{children.toUpperCase()}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
    height: 50,
    width:'70%',
    marginLeft: '15%',
    backgroundColor:  brandColor,
    alignItems: 'center',
    justifyContent: 'center',  
  },
  buttonText: {
    color: '#000000',
    fontSize: 16,
  }
});

export default Button;
