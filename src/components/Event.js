import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const Event = ({ title, description, timeLeft }) => (
  <View style={styles.lessonBlock}>
    <View style={styles.textForLesson}>
      <Text style={styles.subjectInLessonBlock}>
        {title}
      </Text>
      <Text style={styles.isMainSubjectText}>
        {description}
      </Text>
    </View>
    <View style={styles.progressForLesson}>
      <Text style={{ fontSize: 22, color: timeLeft < 10 ? '#ff3d47' : '#30d799' }}>{`${timeLeft} ч.`}</Text>
      <Text style={{ color: 'grey' }}>осталось</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  lessonBlock: {
    flexDirection: 'row',
    height: 75,
    marginHorizontal: '5%',
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#c4c4c4'
  },
  textForLesson: {
    flex: 3
  },
  progressForLesson: {
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subjectInLessonBlock: {
    margin: 4,
    marginLeft: 15,
    fontSize: 20,
    textAlign: 'left'
  },
  isMainSubjectText: {
    margin: 4,
    marginTop: 0,
    marginLeft: 15,
    fontSize: 13,
    textAlign: 'left',
    color: 'grey'
  }
});

export default Event;
