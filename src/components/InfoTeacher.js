import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

const InfoTeacher = ({ avatar, name }) => (
  <View style={styles.container}>
    <Image style={styles.avatar} source={{ uri: avatar }} />
    <Text>{name}</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    paddingRight: 16
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    marginBottom: 8
  }
});

export default InfoTeacher;
