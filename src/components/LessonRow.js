import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

const checked = require('../icons/checked.png');

const LessonRow = ({ title, onPress, complete }) => (
  <TouchableOpacity onPress={onPress} style={styles.lessonBlock}>
    <View style={styles.textForLesson}>
      <Text style={styles.subjectInLessonBlock}>
        {title}
      </Text>
    </View>
    {complete
      ? <View style={styles.progressForLesson}>
          <Image source={checked} style={{ width: 32, height: 32 }} />
        </View>
      : null}
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  lessonBlock: {
    flexDirection: 'row',
    height: 75,
    marginHorizontal: '5%',
    marginTop: 5,
    marginBottom: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#c4c4c4'
  },
  textForLesson: {
    flex: 3,
    justifyContent: 'center'
  },
  progressForLesson: {
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subjectInLessonBlock: {
    margin: 4,
    marginLeft: 15,
    fontSize: 20,
    textAlign: 'left'
  },
  isMainSubjectText: {
    margin: 4,
    marginTop: 0,
    marginLeft: 15,
    fontSize: 13,
    textAlign: 'left',
    color: 'grey'
  }
});

export default LessonRow;
