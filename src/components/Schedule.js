import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Schedule = () => (
  <View style={styles.schedule}>
    <View style={styles.schedule_col_1}>
      <View style={styles.a1}>
        <Text style={styles.day_of_the_week}>
          {' '}
          пн{' '}
        </Text>
        <Text style={styles.day_of_the_week_bold}>
          {' '}
          вт{' '}
        </Text>
        <Text style={styles.day_of_the_week}>
          {' '}
          ср{' '}
        </Text>
        <Text style={styles.day_of_the_week}>
          {' '}
          чт{' '}
        </Text>
      </View>
    </View>
    <View style={styles.schedule_col_2}>
      <Text style={styles.day_of_the_week}>
        {' '}
        Подготовка к занятию{' '}
      </Text>
      <Text style={styles.day_of_the_week_bold}>
        {' '}
        Занятие{' '}
      </Text>
      <Text style={styles.day_of_the_week}>
        {' '}
        Домашняя работа{' '}
      </Text>
      <Text style={styles.day_of_the_week}>
        {' '}
        Подготовка к занятию{' '}
      </Text>
    </View>
    <View style={styles.schedule_col_3}>
      <View style={styles.a1}>
        <Text style={styles.day_of_the_week_bold}>
          {' '}
          пт{' '}
        </Text>
        <Text style={styles.day_of_the_week}>
          {' '}
          сб{' '}
        </Text>
        <Text style={styles.day_of_the_week}>
          {' '}
          вс{' '}
        </Text>
        <Text style={styles.day_of_the_week} />
      </View>
    </View>
    <View style={styles.schedule_col_4}>
      <Text style={styles.day_of_the_week_bold}>
        {' '}
        Занятие{' '}
      </Text>
      <Text style={styles.day_of_the_week}>
        {' '}
        Домашняя работа{' '}
      </Text>
      <Text style={styles.day_of_the_week} />
      <Text style={styles.day_of_the_week} />
    </View>
  </View>
);

const styles = StyleSheet.create({
  schedule: {
    flex: 3,
    paddingTop: 15,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingLeft: 15
  },
  schedule_col_1: {
    flex: 1,
    marginRight: 5
  },
  a1: {
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderColor: '#808080'
  },
  schedule_col_2: {
    flex: 7
  },
  schedule_col_3: {
    flex: 1,
    marginRight: 5
  },
  schedule_col_4: {
    flex: 6
  },
  day_of_the_week: {
    fontSize: 12,
    marginBottom: 5
  },
  day_of_the_week_bold: {
    fontWeight: 'bold',
    marginBottom: 5
  }
});

export default Schedule;
