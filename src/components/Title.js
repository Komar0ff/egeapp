import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Title = ({ title, description }) => (
  <View style={styles.header_row}>
    <Text style={styles.header_light}>
      {description}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  header_row: {
    paddingLeft: '5%',
    paddingVertical: 16
  },
  header_light: {
    fontSize: 18,
    color: '#000'
  }
});

export default Title;
