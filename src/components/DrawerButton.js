import React from 'react';
import { TouchableOpacity, Image, Text } from 'react-native';

const DrawerButton = ({ onPress, Icon, children }) => (
  <TouchableOpacity
    onPress={onPress}
    style={styles.button}
  >
    <Image
      source={Icon}
      style={{ width: 24, height: 24, marginRight: 16 }}
    />
    <Text style={styles.buttonTitle}>{children}</Text>
  </TouchableOpacity>
);

const styles = {
  button: {
    flexDirection: 'row',
    height: 48,
    alignItems: 'center'
  },
  buttonTitle: {
    fontSize: 18
  }
};

export default DrawerButton;
