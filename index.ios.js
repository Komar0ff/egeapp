import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';

import store from './src/store/configureStore';
import rootReducer from './src/reducers/root';
import AppWithNavigationState from './src/navigation/router';

export default class egeapp2 extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('egeapp2', () => egeapp2);
